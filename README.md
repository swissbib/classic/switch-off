# Swissbib - Switch Off
A simple website informing users about the switch off, of swissbib and redirecting them to swisscovery.

- [Internal Documentation](https://intranet.unibas.ch/display/UBIT/Swissbib+-+Switch+Off)
